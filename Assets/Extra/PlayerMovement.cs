﻿using Assets.Source.Player;
using Assets.Source.utils;
using UnityEngine;
using UnityEngine.InputSystem;
using Quaternion = UnityEngine.Quaternion;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

[RequireComponent(typeof(MovementAttribute))]
[RequireComponent(typeof(JumpAttribute))]
public class PlayerMovement : RigidbodyCharacterController
{
    public PlayerControls playerControls;

    #region Look Properties

    [Header("Look")]

    [SerializeField]
    [Range(0, 1)]
    private float mouseSensitivityX = .06f;

    [SerializeField]
    [Range(0, 1)]
    private float mouseSensitivityY = .06f;

    [SerializeField]
    [Range(-90, 90)]
    private float mouseVerticalRangeMinimumDegrees = -90f;

    [SerializeField]
    [Range(-90, 90)]
    private float mouseVerticalRangeMaximumDegrees = 90f;

    [SerializeField]
    private GameObject headObject = null;

    [SerializeField]
    private Camera cameraObject = null;

    private float eulerYawRotation;
    private float eulerPitchRotation;

    #endregion

    #region Movement

    private MovementAttribute movementAttribute;

    #endregion

    #region Jump

    private JumpAttribute jumpAttribute;
    private bool requestingJump = false;

    #endregion

    #region Helpers

    public Vector3 GetBaseGravityNormal()
    {
        return -localUp;
    }

    public float GetMaxFloorAngle()
    {
        return maxFloorAngle;
    }

    public void SetBaseGravityNormal(Vector3 newBaseGravityNormal)
    {
        localUp = -newBaseGravityNormal;
    }

    #endregion

    private Vector2 input { get; set; }
    private Vector2 deltaLook { get; set; }

    protected override void Awake()
    {
        BindPlayerControls();
        Cursor.lockState = CursorLockMode.Locked;
        base.Awake();
    }

    private void Start()
    {
        movementAttribute = GetComponent<MovementAttribute>();
        jumpAttribute = GetComponent<JumpAttribute>();
    }

    private void BindPlayerControls()
    {
        //playerControls = new PlayerControls();
        //playerControls.Player.Look.performed += ctx => SetLook(ctx);
        //playerControls.Player.Move.performed += ctx => SetMove(ctx);
        //playerControls.Player.Jump.performed += SetJump;
    }

    private void OnEnable()
    {
        //playerControls.Enable();
    }

    private void OnDisable()
    {
        //playerControls.Disable();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        Look(deltaLook.x, deltaLook.y);
    }

    public void SetMove(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.phase == InputActionPhase.Performed)
        {
            input = callbackContext.ReadValue<Vector2>();
        }
    }

    public void SetLook(InputAction.CallbackContext callbackContext)
    {
        deltaLook = callbackContext.ReadValue<Vector2>();
    }

    public void SetJump(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.phase == InputActionPhase.Performed)
        {
            requestingJump = true;
        }
    }

    public void SetRespawn(InputAction.CallbackContext callbackContext)
    {
        transform.position = Vector3.zero + Vector3.up * 5;
        localUp = Vector3.up;
        GetComponent<Rigidbody>().velocity = Vector3.zero;
    }

    protected override void FixedUpdate()
    {

        if (requestingJump)
        {
            jumpAttribute.Jump();
            requestingJump = false;
        }

        movementAttribute.movementInput = input;
        movementAttribute.playerLookDirection = headObject.transform.forward;

        base.FixedUpdate();
    }

    // ReSharper disable once UnusedParameter.Local
    private void Look(float deltaX, float deltaY)
    {
        // Rotate along yaw with mouse X axis input
        eulerYawRotation += deltaX * mouseSensitivityX;
        eulerYawRotation = SidekickUtilities.ClampAngle(eulerYawRotation,
            -360f,
            360f);

        eulerPitchRotation -= deltaY * mouseSensitivityY;
        eulerPitchRotation = SidekickUtilities.ClampAngle(eulerPitchRotation,
            mouseVerticalRangeMinimumDegrees,
            mouseVerticalRangeMaximumDegrees);

        Quaternion xQuaternion = Quaternion.AngleAxis(eulerYawRotation, Vector3.up);
        Quaternion yQuaternion = Quaternion.AngleAxis(eulerPitchRotation, Vector3.right);
        headObject.transform.localRotation = xQuaternion;
        cameraObject.transform.localRotation = yQuaternion;
    }
}
