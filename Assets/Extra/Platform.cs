﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Platform : MonoBehaviour
{
    Rigidbody rigidBody;

    #region Rotation

    [Header("Rotation")]

    [SerializeField]
    [Tooltip("Angular Velocity is represented here in degrees")]
    private Vector3 angularVelocity = Vector3.zero;
    private Quaternion currentRotation = Quaternion.identity;

    #endregion

    #region Translation

    [Header("Translation")]

    [SerializeField]
    public List<Transform> pathPoints = new List<Transform>();
    [SerializeField]
    public float speed = 5f;
    private int currentTransformIndex = 0;

    #endregion

    public Vector3 velocity => GetVelocity().normalized * rigidBody.velocity.magnitude;
    private Vector3 storedVelocity = Vector3.zero;
    public bool localRotation = false;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        currentTransformIndex = 0;
        currentRotation = rigidBody.rotation;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Translation
        storedVelocity = GetVelocity();
        rigidBody.MovePosition(gameObject.transform.position + storedVelocity * Time.fixedDeltaTime);

        // Rotation
        var deltaRotation = angularVelocity * Time.fixedDeltaTime;

        var rotation = deltaRotation.normalized;

        if (localRotation)
        {
            rotation = rigidBody.transform.InverseTransformDirection(deltaRotation.normalized);
        }

        currentRotation = rigidBody.rotation * Quaternion.AngleAxis(deltaRotation.magnitude, rotation);
        rigidBody.MoveRotation(currentRotation);
    }

    private Vector3 GetVelocity()
    {
        if (pathPoints.Count > 0)
        {
            var targetPathPoint = pathPoints[currentTransformIndex];
            var worldPosition = transform.position;
            var worldTargetPosition = targetPathPoint.transform.position;
            if ((storedVelocity * Time.fixedDeltaTime).magnitude > (worldTargetPosition - worldPosition).magnitude || worldPosition == targetPathPoint.position)
            {
                currentTransformIndex = currentTransformIndex == (pathPoints.Count - 1) ? 0 : currentTransformIndex + 1;
                targetPathPoint = pathPoints[currentTransformIndex];
                worldTargetPosition = targetPathPoint.transform.position;
            }

            return (worldTargetPosition - worldPosition).normalized * speed;
        }

        else
        {
            return Vector3.zero;
        }
    }
}
