﻿using Assets.Source.Utils.Extensions;
using System;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(PlayerMovement))]
public class GravityBoots : MonoBehaviour
{
    PlayerMovement playerMovement;
    bool lookingForGround = false;
    float flooringResolution = 0.1f;
    Collider gravityRamp = null;

    private void Start()
    {
        playerMovement = GetComponent<PlayerMovement>();
    }

    public void FixedUpdate()
    {

        if (!playerMovement.isGrounded)
        {
            if (gravityRamp != null)
            {
                CollisionDidExit(gravityRamp);
                gravityRamp = null;
            }
            return;
        }

        if (playerMovement.groundingCollider == gravityRamp)
        {
            CollisionDidStay(gravityRamp);
            return;
        }

        if (gravityRamp != null)
        {
            CollisionDidExit(gravityRamp);
        }

        gravityRamp = playerMovement.groundingCollider;
        CollisionDidEnter(gravityRamp);
    }

    public void CollisionDidEnter(Collider collider)
    {
        if (collider.transform.tag == "GravityRamp")
        {
            ConnectPlayerToCollision(collider);
        }

        else if (lookingForGround)
        {
            ConnectPlayerToCollision(collider);
            playerMovement.SetBaseGravityNormal(RoundOutFlooringContact(playerMovement.GetBaseGravityNormal()));
            lookingForGround = false;
        }
    }

    public void CollisionDidStay(Collider collider)
    {
        if (collider.transform.tag == "GravityRamp")
        {
            ConnectPlayerToCollision(collider);
        }

        else if (lookingForGround)
        {
            ConnectPlayerToCollision(collider);
            playerMovement.SetBaseGravityNormal(RoundOutFlooringContact(playerMovement.GetBaseGravityNormal()));
            lookingForGround = false;
        }
    }

    public void CollisionDidExit(Collider collider)
    {
        if (collider.transform.tag == "GravityRamp")
        {
            lookingForGround = true;
        }
    }

    private void ConnectPlayerToCollision(Collider collider)
    {
        var average = playerMovement.groundingContactNormal;

        var angle = (playerMovement.GetBaseGravityNormal() * -1).Angle(average);

        if (angle <= playerMovement.GetMaxFloorAngle())
        {
            playerMovement.SetBaseGravityNormal(-average);
        }
    }

    private Vector3 RoundOutFlooringContact(Vector3 v)
    {
        return new Vector3(((float)Math.Round(v.x / flooringResolution)) * flooringResolution, ((float)Math.Round(v.y / flooringResolution)) * flooringResolution, ((float)Math.Round(v.z / flooringResolution)) * flooringResolution);
    }
}
