using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class RollerBall : MonoBehaviour
{
    Rigidbody storedRigidbody;

    public Vector3 velocity => storedRigidbody.velocity;

    public Vector3 angularVelocity => storedRigidbody.angularVelocity;

    // Start is called before the first frame update
    void Start()
    {
        storedRigidbody = GetComponent<Rigidbody>();   
    }
}
