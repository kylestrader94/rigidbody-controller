﻿using Assets.Source.Utils.Extensions;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Source.Player
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(CapsuleCollider))]
    public abstract class RigidbodyCharacterController : MonoBehaviour
    {
        #region Components

        [Header("Components")]

        [SerializeField]
        protected GameObject playerInternals = null;

        private Rigidbody storedRigidbody = null;

        private CapsuleCollider storedCapsuleCollider = null;

        [SerializeField]
        private GameObject swivelObject = null;

        #endregion

        [SerializeField]
        public Vector3 localUp = Vector3.up;

        [SerializeField]
        [Range(0, 90)]
        public float maxFloorAngle = 45f;

        // The best candidate for a grounding raycast hit based on a sweep done each tick 
        private RaycastHit? groundingRaycastHit;

        private Rigidbody groundingRigidbody => groundingCollider?.GetComponent<Rigidbody>();

        // Several checks must be performed to ensure that a raycast hit is valid
        public bool isGrounded => groundingRaycastHit != null 
            && groundingRaycastHit.Value.point != Vector3.zero 
            && groundingRaycastHit.Value.collider != null;

        public bool isGroundedOnRigidbody => isGrounded && groundingRigidbody != null;

        public bool isGroundedOnKinematic => isGroundedOnRigidbody && groundingRigidbody.isKinematic;

        public Collider groundingCollider => groundingRaycastHit?.collider;

        // Stored rigidbody accessors
        public Vector3 groundingContactNormal => groundingRaycastHit?.normal ?? localUp;
        public Vector3? groundingContactPoint => groundingRaycastHit?.point;
        public Vector3 currentVelocity => storedRigidbody.velocity;
        public Vector3 currentPosition => storedRigidbody.position;
        public float drag => storedRigidbody.drag;
        public float kineticEnergy => storedRigidbody.GetKineticEnergy();
        public bool canSleep => storedRigidbody.GetCanSleep();

        // Stored capsule collider accessors
        public float capsuleColliderRadius => storedCapsuleCollider.radius;
        public float capsuleColliderHeight => storedCapsuleCollider.height;

        [HideInInspector]
        public Vector3 currentInternalVelocity { get; private set; } = Vector3.zero;

        [HideInInspector]
        public Vector3 currentGravitationalVelocity { get; private set; } = Vector3.zero;

        [HideInInspector]
        public Vector3 currentAppliedVelocity { get; private set; } = Vector3.zero;

        [HideInInspector]
        public Vector3 lastAppliedVelocity { get; private set; } = Vector3.zero;

        [HideInInspector]
        public Vector3 currentExternalAngularVelocity { get; private set; } = Vector3.zero;

        protected float targetLocalRotationSlerpStep = 0.04f;

        public LayerMask layerMask => gameObject.layer;

        private Vector3 playerToRigidbodyOffset = Vector3.zero;

        [SerializeField]
        [Tooltip("Time in seconds to complete offset adjustment")]
        private float playerToRigidbodyOffsetAdjustmentSpeed = 0.2f;

        [SerializeField]
        public List<RigidbodyCharacterAttribute> Attributes;

        protected virtual void Awake()
        {
            storedRigidbody = GetComponent<Rigidbody>();
            storedCapsuleCollider = GetComponent<CapsuleCollider>();

            // Totally disable rigidbody sleeping. This ensures that our ground detection works 
            // consistently
            storedRigidbody.sleepThreshold = 0.0f;

            storedRigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
        }

        protected virtual void Update()
        {
            // TODO: This is sloppy
            playerInternals.transform.localPosition = playerToRigidbodyOffset;
            var deltaOffset = (playerToRigidbodyOffset * -1).normalized * (Time.deltaTime / playerToRigidbodyOffsetAdjustmentSpeed);
            if (deltaOffset.magnitude > playerToRigidbodyOffset.magnitude)
            {
                deltaOffset = playerToRigidbodyOffset * -1;
            }

            playerToRigidbodyOffset += deltaOffset;

            var currentUp = playerInternals.transform.up;
            var targetUp = storedRigidbody.transform.up;

            if (currentUp != targetUp)
            {
                var currentRotation = playerInternals.transform.rotation;

                var right = currentUp.Cross(targetUp);
                var angle = Vector3.Angle(currentUp, targetUp);
                var targetForward = Quaternion.AngleAxis(angle, right) * playerInternals.transform.forward;
                var targetRotation = Quaternion.LookRotation(targetForward, targetUp);

                // TODO: Hard-coded rotation LERP
                var updatedRotation = Quaternion.Lerp(currentRotation, targetRotation, Time.deltaTime * 12);

                playerInternals.transform.rotation = updatedRotation;
            }

            swivelObject.transform.Rotate((currentExternalAngularVelocity * Time.deltaTime).Project(Vector3.up));
        }

        protected virtual void FixedUpdate()
        {
            groundingRaycastHit = FindGroundingRaycastHit();

            // Rotate the player so that they are facing up-right according to their gravity normal
            if (storedRigidbody.transform.up != localUp)
            {
                var targetRotation = Quaternion.FromToRotation(
                    storedRigidbody.transform.up,
                    localUp)
                    * storedRigidbody.transform.rotation;

                var diff = Quaternion.Inverse(storedRigidbody.rotation) * targetRotation;

                storedRigidbody.rotation = targetRotation;
                playerInternals.transform.rotation *= Quaternion.Inverse(diff);
            }

            currentInternalVelocity = Vector3.zero;
            currentGravitationalVelocity = Vector3.zero;
            currentAppliedVelocity = Vector3.zero;
            currentExternalAngularVelocity = Vector3.zero;

            currentInternalVelocity = storedRigidbody.velocity;

            var accumulator = new RigidbodyCharacterAttributeResult();

            // Build accumulator
            foreach (var attribute in Attributes)
            {
                attribute.AttributeFixedUpdate(ref accumulator, Time.fixedDeltaTime);
            }

            currentInternalVelocity += accumulator.internalVelocity / (1 - Time.fixedDeltaTime * storedRigidbody.drag);
            currentGravitationalVelocity += accumulator.gravitationalVelocity / (1 - Time.fixedDeltaTime * storedRigidbody.drag);
            currentAppliedVelocity += accumulator.externallyAppliedVelocity / (1 - Time.fixedDeltaTime * storedRigidbody.drag);
            currentExternalAngularVelocity += accumulator.externallyAppliedAngularVelocity;

            // Post op applied angular velocity
            foreach (var postOp in accumulator.externallyAppliedAngularVelocityPostMutators)
            {
                currentExternalAngularVelocity = postOp(currentExternalAngularVelocity);
            }

            // Post op applied velocity
            foreach (var postOp in accumulator.externallyAppliedVelocityPostMutators)
            {
                currentAppliedVelocity = postOp(currentAppliedVelocity);
            }

            // Post op gravitational velocity
            foreach (var postOp in accumulator.gravitationalVelocityPostMutators)
            {
                currentGravitationalVelocity = postOp(currentGravitationalVelocity);
            }

            // Post op internal velocity
            foreach (var postOp in accumulator.internalVelocityPostMutators)
            {
                currentInternalVelocity = postOp(currentInternalVelocity);
            }

            storedRigidbody.transform.position += accumulator.displacement;
            playerToRigidbodyOffset += playerInternals.transform.InverseTransformDirection(-accumulator.displacement);

            var newVelocity = currentInternalVelocity + currentGravitationalVelocity + currentAppliedVelocity;

            lastAppliedVelocity = currentAppliedVelocity;

            storedRigidbody.velocity = newVelocity;
        }

        // Determine if this collision can be considered a grounding collision or an airborne 
        // collision.
        private RaycastHit? FindGroundingRaycastHit()
        {
            var hits = storedCapsuleCollider.SweepAll(layerMask);
            var targetUpNormal = localUp;
            RaycastHit? raycastHit = null;
            var angle = 45;

            foreach (var hit in hits)
            {
                Debug.DrawLine(hit.point, hit.point + hit.normal, Color.red, 5);
                var hitAngle = Mathf.Abs(Vector3.Angle(targetUpNormal, hit.normal));

                if (hitAngle <= angle)
                {
                    raycastHit = hit;
                }
            }

            return raycastHit;
        }
    }
}
