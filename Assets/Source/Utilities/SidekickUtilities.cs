﻿
using Assets.Source.Utils.Extensions;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Source.utils
{
    public static class SidekickUtilities
    {
        public static float ClampAngle(float angle, float minAngle, float maxAngle)
        {
            if (angle < -360f)
            {
                angle += 360f;
            }

            if (angle > 360f)
            {
                angle -= 360f;
            }

            return Mathf.Clamp(angle, minAngle, maxAngle);
        }

        public static RaycastHit FindHitFarthestAlongProjection(List<RaycastHit> hits, Vector3 projection)
        {
            return hits.Aggregate((hitPoint1, hitPoint2) =>
            {
                var hitPoint1RelativeHeight = hitPoint1.point.DistanceAlongProjection(projection);
                var hitPoint2RelativeHeight = hitPoint2.point.DistanceAlongProjection(projection);
                return hitPoint1RelativeHeight > hitPoint2RelativeHeight ? hitPoint1 : hitPoint2;
            });
        }

        public static ContactPoint FindHitFarthestAlongProjection(List<ContactPoint> contacts, Vector3 projection)
        {
            return contacts.Aggregate((c1, c2) =>
            {
                var contact1RelativeHeight = c1.point.DistanceAlongProjection(projection);
                var contact2RelativeHeight = c2.point.DistanceAlongProjection(projection);
                return contact1RelativeHeight > contact2RelativeHeight ? c1 : c2;
            });
        }
    }
}