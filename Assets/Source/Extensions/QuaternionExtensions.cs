﻿using UnityEngine;

namespace Assets.Source.Utils.Utilities
{
    public class QuaternionExtensions
    {
        public static Quaternion Multiply(Quaternion input, float scalar)
        {
            return new Quaternion(input.x * scalar, input.y * scalar, input.z * scalar, input.w * scalar);
        }
    }
}
