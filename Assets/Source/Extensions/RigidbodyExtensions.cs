﻿using UnityEngine;

public static class RigidbodyExtensions
{
    public static float GetKineticEnergy(this Rigidbody rigidbody) => rigidbody.velocity.sqrMagnitude * 0.5f + rigidbody.angularVelocity.sqrMagnitude * 0.5f;

    public static bool GetCanSleep(this Rigidbody rigidbody) => rigidbody.GetKineticEnergy() < Physics.sleepThreshold;
}