﻿using UnityEngine;

namespace Assets.Source.Utils.Extensions
{
    public static class Vector3Extensions
    {
        #region Masking

        public static Vector3 MaskX(this Vector3 vec, float xValue)
        {
            return new Vector3(xValue, vec.y, vec.z);
        }

        public static Vector3 MaskXY(this Vector3 vec, float xValue, float yValue)
        {
            return new Vector3(xValue, yValue, vec.z);
        }

        public static Vector3 MaskXZ(this Vector3 vec, float xValue, float zValue)
        {
            return new Vector3(xValue, vec.y, zValue);
        }

        public static Vector3 MaskY(this Vector3 vec, float yValue)
        {
            return new Vector3(vec.x, yValue, vec.z);
        }

        public static Vector3 MaskYZ(this Vector3 vec, float yValue, float zValue)
        {
            return new Vector3(vec.x, yValue, zValue);
        }

        public static Vector3 MaskZ(this Vector3 vec, float zValue)
        {
            return new Vector3(vec.x, vec.y, zValue);
        }

        #endregion

        public static bool IsNonZero(this Vector3 vec)
        {
            return vec != Vector3.zero;
        }

        public static Vector3 FloorDeadZone(this Vector3 vec, float deadzone)
        {
            return new Vector3(
                Mathf.Abs(vec.x) < deadzone ? 0 : vec.x,
                Mathf.Abs(vec.x) < deadzone ? 0 : vec.y,
                Mathf.Abs(vec.x) < deadzone ? 0 : vec.z);
        }

        public static float Angle(this Vector3 vector, Vector3 vectorToCompare)
        {
            return Vector3.Angle(vector, vectorToCompare);
        }

        public static Vector3 Cross(this Vector3 v1, Vector3 v2)
        {
            return Vector3.Cross(v1, v2);
        }

        public static float Dot(this Vector3 v1, Vector3 v2)
        {
            return Vector3.Dot(v1, v2);
        }

        public static Vector3 Project(this Vector3 v1, Vector3 v2)
        {
            return Vector3.Project(v1, v2);
        }

        public static Vector3 ProjectSubtractive(this Vector3 v1, Vector3 v2)
        {
            return v1 - v1.Project(v2);
        }

        public static Vector3 PositiveProjection(this Vector3 v1, Vector3 v2)
        {
            return v1.Dot(v2) > 0 ? v1.Project(v2) : Vector3.zero;
        }

        public static Vector3 NegativeProjection(this Vector3 v1, Vector3 v2)
        {
            return v1.Dot(v2) < 0 ? v1.Project(v2) : Vector3.zero;
        }

        public static float DistanceAlongProjection(this Vector3 v1, Vector3 v2)
        {
            var proj = v1.Project(v2);
            return proj.Dot(v2) < 0 ? proj.magnitude * -1 : proj.magnitude;
        }

        public static Vector3 ProjectOnPlane(this Vector3 vector, Vector3 planeNormal)
        {
            return Vector3.ProjectOnPlane(vector, planeNormal);
        }

        public static Vector3 MultiplyComponents(this Vector3 vec, Vector3 other)
        {
            return new Vector3(vec.x * other.x, vec.y * other.y, vec.z * other.z);
        }

        public static Vector3 DivideComponents(this Vector3 vec, Vector3 other)
        {
            return new Vector3(vec.x * other.x, vec.y * other.y, vec.z * other.z);
        }

        public static Vector3 Reflect(this Vector3 inDirection, Vector3 inNormal)
        {
            return Vector3.Reflect(inDirection, inNormal);
        }

        public static Vector3 RotatePointAround(this Vector3 point, Vector3 pivotPoint, Vector3 axis, float angle)
        {
            var direction = point - pivotPoint;
            var newDirection = Quaternion.AngleAxis(angle, axis) * direction;
            return pivotPoint + newDirection;
        }

        public static Vector3 RotateDirectionAround(this Vector3 direction, Vector3 axis, float angle)
        {
            return Quaternion.AngleAxis(angle, axis) * direction;
        }

        public static Vector3 CapAlongAxis(this Vector3 vector, Vector3 axis, float magnitude)
        {
            var axisComponent = vector.Project(axis);
            var remainderComponent = vector - axisComponent;
            var verticalMagnitude = Mathf.Min(axisComponent.magnitude, magnitude);

            return axisComponent.normalized * verticalMagnitude + remainderComponent;
        }
    }
}
