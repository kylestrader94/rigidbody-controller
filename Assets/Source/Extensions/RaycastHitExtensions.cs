﻿
using UnityEngine;

public static class RaycastHitExtensions
{
    public static bool IsValidHit(this RaycastHit raycastHit) => raycastHit.collider != null && raycastHit.point != Vector3.zero;
}
