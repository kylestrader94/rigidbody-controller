﻿using System;

public static class NullableExtensions
{
    public static void Map<T>(this T t, Action<T> action) where T : class
    {
        if (t != null)
        {
            action(t);
        }
    }

    public static void MapComponent<T>(this T t, Action<T> action) where T: UnityEngine.Object
    {
        if (t != null)
        {
            action(t);
        }
    }
}
