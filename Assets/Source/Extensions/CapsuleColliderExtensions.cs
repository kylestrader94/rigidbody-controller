﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class CapsuleColliderExtensions
{
    public static Vector3 GetPoint1(this CapsuleCollider capsuleCollider)
    {
        var up = capsuleCollider.transform.up;
        var distanceFromCenter = capsuleCollider.height / 2 - capsuleCollider.radius;
        return capsuleCollider.transform.position + up * distanceFromCenter;
    }

    public static Vector3 GetPoint2(this CapsuleCollider capsuleCollider)
    {
        var down = -capsuleCollider.transform.up;
        var distanceFromCenter = capsuleCollider.height / 2 - capsuleCollider.radius;
        return capsuleCollider.transform.position + down * distanceFromCenter;
    }

    public static List<Collider> Overlap(this CapsuleCollider capsuleCollider, LayerMask layerMask, float skin = 0f)
    {
        var radius = capsuleCollider.radius + skin;
        return Physics.OverlapCapsule(capsuleCollider.GetPoint1(), capsuleCollider.GetPoint2(), radius, layerMask).ToList();
    }

    public static List<RaycastHit> SweepAll(this CapsuleCollider capsuleCollider, LayerMask layerMask, float skin = 0f)
    {
        var direction = -capsuleCollider.transform.up;
        var radius = capsuleCollider.radius - (Physics.defaultContactOffset * 2);
        var offset = ((capsuleCollider.height - (radius * 2)) / 2) + Physics.defaultContactOffset;
        var hits = Physics.SphereCastAll(capsuleCollider.transform.position, radius, direction, offset, layerMask);

        return hits.Where(hit => { return hit.collider != null && (hit.point != Vector3.zero || hit.distance > 0); }).ToList();
    }
}
