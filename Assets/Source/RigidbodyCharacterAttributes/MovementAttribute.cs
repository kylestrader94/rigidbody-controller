﻿
using Assets.Source.Player;
using Assets.Source.Utils.Extensions;
using UnityEngine;

[RequireComponent(typeof(RigidbodyCharacterController))]
public class MovementAttribute : RigidbodyCharacterAttribute
{
    private RigidbodyCharacterController characterController;

    [SerializeField]
    [Range(0, 1000)]
    private float _groundedAcceleration = 27f;
    public float groundedAcceleration
    {
        get => _groundedAcceleration;
        private set => _groundedAcceleration = value;
    }

    [SerializeField]
    [Range(0, 1000)]
    private float _groundedStoppingAcceleration = 54f;
    public float groundedStoppingAcceleration
    {
        get => _groundedStoppingAcceleration;
        private set => _groundedStoppingAcceleration = value;
    }

    [SerializeField]
    [Range(0, 100)]
    private float _airborneAcceleration = 10f;
    public float airborneAcceleration
    {
        get => _airborneAcceleration;
        private set => _airborneAcceleration = value;
    }

    [SerializeField]
    [Range(0, 100)]
    private float _maximumGroundedSpeed = 7f;
    public float maximumGroundedSpeed
    {
        get => _maximumGroundedSpeed;
        private set => _maximumGroundedSpeed = value;
    }

    [SerializeField]
    [Range(0, 100)]
    private float _maximumAirborneSpeed = 13.4f;
    public float maximumAirborneSpeed
    {
        get => _maximumAirborneSpeed;
        private set => _maximumAirborneSpeed = value;
    }

    [HideInInspector]
    public Vector2 movementInput;
    [HideInInspector]
    public Vector3 playerLookDirection;

    private void Start()
    {
        characterController = GetComponent<RigidbodyCharacterController>();
    }

    public override void AttributeFixedUpdate(ref RigidbodyCharacterAttributeResult result, float deltaTime)
    {
        var movementDirection = GetMovementDirection(
            playerLookDirection,
            result.overrideGroundingContactNormal ?? characterController.groundingContactNormal, 
            movementInput
        );

        var targetUpNormal = characterController.localUp;
        var computedGravityNormal = -targetUpNormal;

        GetTargetVelocityAndAcceleration(
            movementDirection,
            result.overrideIsGrounded || characterController.isGrounded,
            out float targetSpeed,
            out float targetAcceleration
        );

        // Apply input magnitude
        targetSpeed *= Mathf.Min(1f, movementDirection.magnitude);

        // Compensate for drag
        targetSpeed /= 1 - deltaTime * characterController.drag;

        Vector3 accumualtedVelocityChange = Vector3.zero;

        var storedVelocity = characterController.currentVelocity;

        if (characterController.lastAppliedVelocity.IsNonZero())
        {
            storedVelocity -= characterController.lastAppliedVelocity * (1 - Time.fixedDeltaTime * characterController.drag);
        }

        //if (result.overrideIsGrounded ?? characterController.isGrounded)
        {
            var frictionAcceleration = DetermineRequiredFrictionAcceleration(movementDirection, storedVelocity, result.overrideGroundingContactNormal ?? characterController.groundingContactNormal, result.overrideIsGrounded || characterController.isGrounded);
            var right = frictionAcceleration.Cross(computedGravityNormal);
            var forward = right.Cross(computedGravityNormal);
            frictionAcceleration = frictionAcceleration.Project(forward);
            accumualtedVelocityChange += frictionAcceleration * deltaTime;
        }

        // Check if the player is headed in the right direction at the target velocity. If not, 
        // lets get them there
        var scale = DetermineRequiredAccelerationScale(movementDirection,
            storedVelocity,
            targetSpeed,
            targetAcceleration,
            result.overrideIsGrounded || characterController.isGrounded,
            deltaTime);

        accumualtedVelocityChange += movementDirection
            * targetAcceleration
            * deltaTime
            * scale;

        result.internalVelocity += accumualtedVelocityChange;
    }

    // Project the input direction onto the ground that the player is standing on. If the
    // player is climbing a slope then push the player up the hill instead of into it.
    private Vector3 GetMovementDirection(Vector3 playerLookDirection, Vector3 groundingContactNormal, Vector2 movementInput)
    {
        var targetUpNormal = characterController.localUp;
        var headRight = playerLookDirection.Cross(targetUpNormal).normalized;
        var forward = groundingContactNormal.Cross(headRight).normalized;

        // Unity uses a left-handed GL. To get the right vector multiply left by -1.
        var right = forward.Cross(groundingContactNormal).normalized * -1;
        return ((forward * movementInput.y) + (right * movementInput.x)).normalized;
    }

    // Target speed and acceleration changes if the player is grounded or airborne
    private void GetTargetVelocityAndAcceleration(Vector3 movementDirection,
        bool isGrounded,
        out float targetSpeed,
        out float targetAcceleration)
    {
        targetSpeed = GetTargetSpeed(isGrounded);
        targetAcceleration = GetTargetAcceleration(movementDirection, isGrounded);
    }

    private float GetTargetSpeed(bool isGrounded)
    {
        // If player is grounded and is using gravity, calculate velocity using grounded
        // velocities and acceleration
        return isGrounded ? maximumGroundedSpeed : maximumAirborneSpeed;
    }

    private float GetTargetAcceleration(Vector3 movementDirection, bool isGrounded)
    {
        // If player is grounded and is using gravity, calculate velocity using grounded
        // velocities and acceleration
        if (isGrounded)
        {
            return movementDirection.Dot(characterController.currentVelocity) >= 0 ? groundedAcceleration : groundedStoppingAcceleration;
        }

        // Use airborne maximum velocity and acceleration
        else
        {
            return airborneAcceleration;
        }
    }

    // Determine the acceleration of the steering needed to steer the player in the target 
    // direction
    private Vector3 DetermineRequiredFrictionAcceleration(Vector3 movementDirection, Vector3 currentVelocity, Vector3 groundingContactNormal, bool isGrounded)
    {
        if (!isGrounded && movementDirection == Vector3.zero) return Vector3.zero;

        // If the player is moving, apply friction that steers the player character in
        // the direction of the input
        Vector3 groundedVelocity = currentVelocity - currentVelocity.Project(groundingContactNormal);
        Vector3 friction = groundedVelocity.Project(movementDirection) - groundedVelocity;
        Vector3 acceleration = friction.normalized;

        acceleration *= isGrounded ? groundedStoppingAcceleration : airborneAcceleration;

        float scale = Mathf.Min(friction.magnitude / (acceleration.magnitude * Time.fixedDeltaTime), 1f);
        return acceleration * scale;
    }

    // Determine if the velocity will take a full acceleration step or only part of one. This
    // function helps keep us from moving the player faster than the maximum velocity
    private float DetermineRequiredAccelerationScale(Vector3 movementDirection,
        Vector3 currentVelocity,
        float targetSpeed,
        float targetAcceleration,
        bool isGrounded,
        float deltaTime)
    {
        var currentInputProjectedMagnitude = currentVelocity.Project(movementDirection).magnitude;
        float scale;
        if (Vector3.Dot(currentVelocity, movementDirection) <= 0)
        {
            scale = 1f;
        }

        else if (currentInputProjectedMagnitude < targetSpeed)
        {
            // Check to see if the projected velocity after this pass of acceleration is faster 
            // than the target speed. If it is, scale it down so we dont push the player faster 
            // than they should be going
            var velocityChange = movementDirection * targetAcceleration * deltaTime;
            var projectedVelocity = currentVelocity + velocityChange;
            var acceleratedInputProjectedMagnitude = projectedVelocity.Project(movementDirection).magnitude;
            if (acceleratedInputProjectedMagnitude > targetSpeed)
            {
                var targetDiff = targetSpeed - currentInputProjectedMagnitude;
                var acceleratedDiff = acceleratedInputProjectedMagnitude - currentInputProjectedMagnitude;
                scale = targetDiff / acceleratedDiff;
            }

            else
            {
                scale = 1f;
            }
        }

        else if (isGrounded && currentInputProjectedMagnitude > targetSpeed)
        {
            // Check to see if the projected velocity after this pass of acceleration is slower 
            // than the target speed. If it is, scale it down so we dont push the player slower 
            // than they should be going
            var velocityChange = movementDirection * -targetAcceleration * deltaTime;
            var projectedVelocity = currentVelocity + velocityChange;
            var acceleratedInputProjectedMagnitude = projectedVelocity.Project(movementDirection).magnitude;
            if (acceleratedInputProjectedMagnitude < targetSpeed)
            {
                var targetDiff = currentInputProjectedMagnitude - targetSpeed;
                var acceleratedDiff = currentInputProjectedMagnitude - acceleratedInputProjectedMagnitude;
                scale = targetDiff / acceleratedDiff * -1;
            }

            else
            {
                scale = -1f;
            }
        }

        else
        {
            scale = 0;
        }



        return scale;
    }
}
