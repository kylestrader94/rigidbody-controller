﻿using Assets.Source.Player;
using Assets.Source.Utils.Extensions;
using System;
using UnityEngine;

[RequireComponent(typeof(RigidbodyCharacterController))]
class AutoStepAttribute : RigidbodyCharacterAttribute
{
    private RigidbodyCharacterController characterController;

    [SerializeField]
    private float maxAutoStepHeight = 0.4f;

    [SerializeField]
    private float minAutoStepOvershoot = 0.1f;

    private bool wasGrounded = false;

    private void Start()
    {
        characterController = GetComponent<RigidbodyCharacterController>();
    }

    public override void AttributeFixedUpdate(ref RigidbodyCharacterAttributeResult result, float deltaTime)
    {
        var localUp = characterController.localUp;
        var stepUpDisplacement = FindStepHeight(characterController, deltaTime, out RaycastHit? raycastHit);

        // If the player is not stepping this frame, bail out early
        if (stepUpDisplacement == null || raycastHit == null || !raycastHit.Value.IsValidHit())
        {
            result.overrideGroundingRaycastHit = null;
            result.overrideGroundingContactNormal = null;
            wasGrounded = result.overrideIsGrounded || characterController.isGrounded;
            return;
        }


        RaycastHit incomingRaycastHit = raycastHit.Value;

        result.overrideGroundingRaycastHit = incomingRaycastHit;
        result.overrideGroundingContactNormal = localUp;

        if ((result.overrideIsGrounded == false || !characterController.isGrounded) && stepUpDisplacement == null)
        //if (!raycastHit.HasValue || !raycastHit.Value.IsValidHit() || (characterController.groundingCollider == null && characterController.currentInternalVelocity.PositiveProjection(localUp).magnitude > 0))
        {
            result.overrideGroundingRaycastHit = null;
            result.overrideGroundingContactNormal = null;
            return;
        }

        Vector3 offset = Vector3.zero;

        // If the displacement is upwards relative to the player apply the displacement to the offset
        // Apply upwards displacement.
        if (stepUpDisplacement.Value.Dot(localUp) >= 0)
        {
            offset = localUp * (stepUpDisplacement.Value.magnitude);
        }

        // If:
        // 1 - The displacement is downwards relative to the player
        // 2 - The player's internal velocity is not upwards relative to the player
        // 3 - The player is grounded currently, or was in the last frame
        // Apply downwards displacement.
        else if (characterController.localUp.Dot(stepUpDisplacement.Value) <= 0 && (characterController.isGrounded || wasGrounded))
        {
            offset = -localUp * (stepUpDisplacement.Value.magnitude);
        } 
        
        else if (characterController.localUp.Dot(stepUpDisplacement.Value) <= 0)
        {
            return;
        }

        wasGrounded = true;

        result.displacement += offset;

        result.gravitationalVelocityPostMutators.Add((gravitationalVelocity) =>
        {
            if (offset.Dot(characterController.localUp) < 0)
            {
                return gravitationalVelocity;
            }

            return gravitationalVelocity - gravitationalVelocity.PositiveProjection(-characterController.localUp);
        });

        result.internalVelocityPostMutators.Add((internalVelocity) =>
        {
            if (offset.Dot(characterController.localUp) < 0)
            {
                return internalVelocity;
            }

            return internalVelocity - internalVelocity.PositiveProjection(-characterController.localUp);
        });
    }

    private Vector3? FindStepHeight(RigidbodyCharacterController characterController, float deltaTime, out RaycastHit? raycastHit)
    {
        raycastHit = null;

        var velocityOffset = characterController.currentInternalVelocity.ProjectSubtractive(characterController.localUp) * deltaTime;
        var playerBasePosition = characterController.currentPosition - characterController.localUp * (characterController.capsuleColliderHeight / 2);
        var radius = characterController.capsuleColliderRadius * 2;
        var pointOffset = characterController.localUp * ((characterController.capsuleColliderHeight / 2) - radius);
        var searchOffset = characterController.localUp * (maxAutoStepHeight + Physics.defaultContactOffset);
        var point1 = characterController.currentPosition + pointOffset + searchOffset + velocityOffset;
        var point2 = characterController.currentPosition - pointOffset + searchOffset + velocityOffset;

        // Find the highest colliding point along the path of a capsule collider cast offset by the player's velocity that is within an acceptable height (+/- maxAutoStepHeight)
        if (!Physics.CapsuleCast(point1, point2, radius, -characterController.localUp, out RaycastHit targetHit, searchOffset.magnitude * 2, characterController.layerMask))
        {
            return null;
        }

        // The hit's height along the player's local up vector
        var hitProj = targetHit.point.Project(characterController.localUp);
        // The player's base position height along the player's local up vector
        var playerBaseProj = playerBasePosition.Project(characterController.localUp);

        var diff = (hitProj - playerBaseProj).Project(characterController.localUp);

        if (characterController.currentInternalVelocity.Dot(characterController.localUp) > 0 && diff.magnitude > Physics.defaultContactOffset && diff.Dot(characterController.localUp) < 0)
        {
            return null;
        }

        // If grounded and the magnitude of the step offset is greater than the `maxAutoStepHeight`, we should not offset the player to match the height of the step
        if ((hitProj - playerBaseProj).magnitude > maxAutoStepHeight + Physics.defaultContactOffset)
        {
            return null;
        }

        /* If the hit was on a step (right angle surface)
         * 
         *                      |     ^    |
         *                |     |     |    | - *Player*
         *                |      \    |   /
         *                v       \______/
         *               __________________________________________
         *              |  
         *       -----> |   A step should have a top surface that
         *              |   is ~equal~ to the player's local
         *              |   up normal and a front-facing surface
         *              |   that is ~perpendicular~ to the player's
         *              |   local up normal.
         *              
         */

        var downwardCastOrigin = targetHit.point + characterController.localUp * Physics.defaultContactOffset;
        downwardCastOrigin += (targetHit.point - characterController.currentPosition).ProjectSubtractive(characterController.localUp).normalized * minAutoStepOvershoot;

        // TODO: Max Distance should only have to be `Physics.defaultContactOffset`
        if (Physics.Raycast(downwardCastOrigin, -characterController.localUp, out RaycastHit downwardHitInfo, maxAutoStepHeight, characterController.layerMask))
        {
            if (downwardHitInfo.normal.normalized == characterController.localUp)
            {
                if ((hitProj - playerBaseProj).Dot(characterController.localUp) < 0)
                {
                    raycastHit = targetHit;
                    var displacement = hitProj - playerBaseProj;
                    if (displacement.magnitude < Physics.defaultContactOffset)
                    {
                        displacement = Vector3.zero;
                    }
                    return displacement;
                }

                var forwardCastOrigin = targetHit.point + -characterController.localUp * Physics.defaultContactOffset;
                var distanceToPlayer = (targetHit.point - characterController.currentPosition).ProjectSubtractive(characterController.localUp);
                forwardCastOrigin -= distanceToPlayer;

                if (Physics.Raycast(forwardCastOrigin, distanceToPlayer.normalized, out RaycastHit forwardHitInfo, distanceToPlayer.magnitude + Physics.defaultContactOffset, characterController.layerMask))
                {
                    if (Mathf.Abs(forwardHitInfo.normal.normalized.Dot(characterController.localUp)) < Physics.defaultContactOffset)
                    {
                        raycastHit = targetHit;
                        var displacement = hitProj - playerBaseProj;
                        return displacement;
                    }

                    else
                    {
                        return null;
                    }
                }

                else
                {
                    return null;
                }
            }
        }

        // No conditions met, bail out.
        return null;
    }
}
