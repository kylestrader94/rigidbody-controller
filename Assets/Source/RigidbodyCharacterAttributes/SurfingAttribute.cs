﻿using Assets.Source.Player;
using Assets.Source.Utils.Extensions;
using System;
using UnityEngine;

[RequireComponent(typeof(RigidbodyCharacterController))]
public class SurfingAttribute : RigidbodyCharacterAttribute
{
    private RigidbodyCharacterController characterController;

    [SerializeField]
    [Range(0, 2)]
    protected float exitVelocityCoefficient = .7f;

    public float maximumVerticalAcceleration = 26;

    public float maximumHorizontalAcceleration = 27;

    private Vector3 lastAccumulatedVelocity = Vector3.zero;

    private void Start()
    {
        characterController = GetComponent<RigidbodyCharacterController>();
    }

    public override void AttributeFixedUpdate(ref RigidbodyCharacterAttributeResult result, float deltaTime)
    {
        var grounded = result.overrideIsGrounded == true || characterController.isGrounded;

        var groundingRigidbody = result.overrideGroundingRaycastHit?.rigidbody != null 
            ? result.overrideGroundingRaycastHit?.rigidbody 
            : characterController.groundingCollider?.attachedRigidbody;

        var groundingContactPoint = result.overrideGroundingRaycastHit?.point != null
            ? result.overrideGroundingRaycastHit?.point
            : characterController.groundingContactPoint;

        if (!grounded || groundingRigidbody == null || groundingContactPoint == null)
        {
            result.gravitationalVelocity += lastAccumulatedVelocity * exitVelocityCoefficient;
            result.internalVelocity -= lastAccumulatedVelocity * (1 - Time.fixedDeltaTime * characterController.drag);
            lastAccumulatedVelocity = Vector3.zero;
            return;
        }

        var groundingPoint = groundingContactPoint.Value;

        // Angular Velocity / Centripetal Acceleration
        {
            var accumulatedVelocity = Vector3.zero;
            var accumulatedAngularVelocity = Vector3.zero;

            // Tangential Velocity
            var tangentialVelocity = groundingRigidbody.GetPointVelocity(groundingPoint);

            accumulatedVelocity += tangentialVelocity;

            // Angular Velocity
            var localTangentialVelocity = tangentialVelocity - groundingRigidbody.velocity;
            var localAngularVelocity = groundingRigidbody.angularVelocity.Project(characterController.localUp);

            var radius = localAngularVelocity.magnitude == 0 
                ? 0 
                : localTangentialVelocity.magnitude / localAngularVelocity.magnitude;

            var centerPoint = groundingPoint + localAngularVelocity.normalized.Cross(localTangentialVelocity.normalized) * radius;

            var toCenterAdjusted = (centerPoint - groundingPoint).ProjectSubtractive(characterController.localUp);
            var adjustedTangentialVelocity = localTangentialVelocity.ProjectSubtractive(characterController.localUp);

            var angularVelocityAxis = adjustedTangentialVelocity.Cross(toCenterAdjusted).Project(characterController.localUp).normalized;
            var adjustedAngularVelocityAxis = angularVelocityAxis.magnitude * Vector3.up * angularVelocityAxis.Dot(characterController.localUp);
            var angularVelocity = toCenterAdjusted.magnitude == 0 ? Vector3.zero : (adjustedAngularVelocityAxis * (adjustedTangentialVelocity.magnitude / toCenterAdjusted.magnitude) * Mathf.Rad2Deg);

            accumulatedAngularVelocity += angularVelocity;

            // Centripetal Acceleration
            var centripetalAcceleration = toCenterAdjusted.magnitude == 0 ? Vector3.zero : toCenterAdjusted.normalized * (Mathf.Pow(adjustedTangentialVelocity.magnitude, 2) / toCenterAdjusted.magnitude);
            var coef = groundingRigidbody.isKinematic ? -0.0099f : -0.01f;

            accumulatedVelocity += centripetalAcceleration * coef;

            accumulatedVelocity = accumulatedVelocity.CapAlongAxis(characterController.localUp, characterController.currentInternalVelocity.Project(characterController.localUp).magnitude + maximumVerticalAcceleration * Time.fixedDeltaTime);
            accumulatedVelocity = accumulatedVelocity.CapAlongAxis(characterController.currentInternalVelocity.ProjectSubtractive(characterController.localUp).normalized, characterController.currentInternalVelocity.ProjectSubtractive(characterController.localUp).magnitude + maximumHorizontalAcceleration * Time.fixedDeltaTime);

            /* 
             * Should the adapter be asleep? The sleep flag on the adapter's rigidbody will never 
                be flipped since we are constantly applying a downwards acceleration via gravity, 
                so we need to manually check if it would be asleep under normal circumstances.
            */
            var groundingRigidbodyCanSleep = groundingRigidbody.GetCanSleep();
            var controllerCanSleep = characterController.canSleep;

            if (!groundingRigidbodyCanSleep || !controllerCanSleep)
            {
                result.externallyAppliedVelocity += accumulatedVelocity;
                result.externallyAppliedAngularVelocity += accumulatedAngularVelocity;
                Debug.DrawLine(characterController.currentPosition, characterController.currentPosition + accumulatedVelocity * Time.fixedDeltaTime, Color.red, 10);

                //lastAccumulatedVelocity = lastAccumulatedVelocity.CapAlongAxis(characterController.localUp, accumulatedVelocity.Project(characterController.localUp).magnitude);

                result.internalVelocity -= lastAccumulatedVelocity;

                lastAccumulatedVelocity = accumulatedVelocity;
            }
        }
    }
}

