﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class RigidbodyCharacterAttributeResult
{
    public Vector3 internalVelocity = Vector3.zero;
    public List<Func<Vector3, Vector3>> internalVelocityPostMutators = new List<Func<Vector3, Vector3>>();

    public Vector3 gravitationalVelocity = Vector3.zero;
    public List<Func<Vector3, Vector3>> gravitationalVelocityPostMutators = new List<Func<Vector3, Vector3>>();

    public Vector3 externallyAppliedVelocity = Vector3.zero;
    public List<Func<Vector3, Vector3>> externallyAppliedVelocityPostMutators = new List<Func<Vector3, Vector3>>();

    public Vector3 externallyAppliedAngularVelocity = Vector3.zero;
    public List<Func<Vector3, Vector3>> externallyAppliedAngularVelocityPostMutators = new List<Func<Vector3, Vector3>>();

    public Vector3 displacement = Vector3.zero;
    public bool overrideIsGrounded => overrideGroundingRaycastHit != null && overrideGroundingRaycastHit.Value.IsValidHit();
    public Vector3? overrideGroundingContactNormal;
    public RaycastHit? overrideGroundingRaycastHit;
}
