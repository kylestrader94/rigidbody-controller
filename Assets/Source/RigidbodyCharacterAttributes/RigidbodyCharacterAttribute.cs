﻿using System;
using UnityEngine;

[Serializable]
public abstract class RigidbodyCharacterAttribute : MonoBehaviour
{
    public abstract void AttributeFixedUpdate(
        ref RigidbodyCharacterAttributeResult result, 
        float deltaTime
    );
}
