﻿using Assets.Source.Player;
using Assets.Source.Utils.Extensions;
using System;
using UnityEngine;

[Serializable]
[RequireComponent(typeof(RigidbodyCharacterController))]
[RequireComponent(typeof(GravityAttribute))]
public class JumpAttribute: RigidbodyCharacterAttribute
{
    private RigidbodyCharacterController characterController;
    private GravityAttribute gravityAttribute;

    [SerializeField]
    private float jumpHeight = 2.96f;

    [SerializeField]
    private int maxJumpInputBuffer = 2;
    private int jumpInputBufferCounter = 0;
    [SerializeField]
    private int maxJumpGroundedBuffer = 1;
    private int jumpGroundedBuffer = 0;

    [SerializeField]
    private int maxSequentialJumps = 1;
    private int currentSequentialJumps = 0;

    private void Start()
    {
        characterController = GetComponent<RigidbodyCharacterController>();
        gravityAttribute = GetComponent<GravityAttribute>();
    }

    public void Jump()
    {
        jumpInputBufferCounter = maxJumpInputBuffer;
    }

    public override void AttributeFixedUpdate(ref RigidbodyCharacterAttributeResult result, float deltaTime)
    {
        if (result.overrideIsGrounded || characterController.isGrounded)
        {
            if (jumpGroundedBuffer >= maxJumpGroundedBuffer)
            {
                currentSequentialJumps = 0;
            }

            else
            {
                jumpGroundedBuffer++;
            }
        }

        else
        {
            jumpGroundedBuffer = 0;
        }

        if (jumpInputBufferCounter <= 0)
        {
            jumpInputBufferCounter = 0;
            return;
        }

        else if (currentSequentialJumps >= maxSequentialJumps)
        {
            jumpInputBufferCounter--;
            return;
        }

        jumpInputBufferCounter = 0;
        currentSequentialJumps++;

        // Find the initial velocity required to reach our desired jump height
        // Using kinematic equation: Vf^2 = Vi^2 + 2a(deltaY)
        var magnitude = Mathf.Sqrt(2 * gravityAttribute.gravitationalAcceleration * jumpHeight);
        var upVec = characterController.localUp;

        result.overrideGroundingRaycastHit = null;

        result.externallyAppliedVelocityPostMutators.Add((appliedVelocity) =>
        {
            return appliedVelocity + appliedVelocity.PositiveProjection(characterController.localUp);
        });

        result.gravitationalVelocityPostMutators.Add((gravitationalVelocity) =>
        {
            return Vector3.zero;
        });

        result.internalVelocityPostMutators.Add((internalVelocity) =>
        {
            return internalVelocity.ProjectSubtractive(characterController.localUp) + characterController.localUp * magnitude;
        });
    }
}
