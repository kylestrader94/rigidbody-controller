﻿using Assets.Source.Player;
using Assets.Source.Utils.Extensions;
using UnityEngine;

[RequireComponent(typeof(RigidbodyCharacterController))]
public class GravityAttribute : RigidbodyCharacterAttribute
{
    private RigidbodyCharacterController characterController;

    [SerializeField]
    private bool _useGravity = false;
    public bool useGravity { 
        get => _useGravity; 
        private set => _useGravity = value; 
    }

    [SerializeField] 
    private float _gravitationalAcceleration = 37f;
    public float gravitationalAcceleration { 
        get => _gravitationalAcceleration; 
        private set => _gravitationalAcceleration = value; 
    }

    [SerializeField]
    [Range(0, 100)]
    private float _maximumFallingSpeed = 62.9f;
    public float maximumFallingSpeed { 
        get => _maximumFallingSpeed; 
        private set => _maximumFallingSpeed = value; 
    }

    private void Start()
    {
        characterController = GetComponent<RigidbodyCharacterController>();
    }

    public override void AttributeFixedUpdate(ref RigidbodyCharacterAttributeResult result, float deltaTime)
    {
        // If the player is not grounded and the controller uses gravity then apply a constant 
        // downward acceleration
        if (useGravity)
        {

            // Are we already falling at max speed?
            var fallingAtMaxSpeed = characterController.currentVelocity.PositiveProjection(-characterController.localUp).magnitude >= maximumFallingSpeed;

            var controllerCanSleep = characterController.canSleep;
            var controllerIsGrounded = result.overrideIsGrounded == true || characterController.isGrounded;
            var controllerGroundedOnNonKinematicRigidbody = characterController.isGroundedOnRigidbody && !characterController.isGroundedOnKinematic;

            if (!fallingAtMaxSpeed && (!controllerCanSleep || !controllerIsGrounded || controllerGroundedOnNonKinematicRigidbody))
            {
                var contactNormal = result.overrideGroundingContactNormal ?? characterController.groundingContactNormal;
                var hangingOnEdge = false;

                if (controllerIsGrounded && controllerGroundedOnNonKinematicRigidbody)
                {
                    var currentPosition = characterController.currentPosition;
                    var contactPoint = characterController.groundingContactPoint.Value;

                    var toRaycastOrigin = (currentPosition - contactPoint).ProjectSubtractive(characterController.localUp);
                    var raycastOrigin = contactPoint + toRaycastOrigin;

                    var angle = Vector3.Angle(characterController.localUp, contactNormal);
                    var raycastDistance = toRaycastOrigin.magnitude * Mathf.Tan(angle * Mathf.Deg2Rad) + Physics.defaultContactOffset;

                    Physics.Raycast(raycastOrigin, -characterController.localUp, out RaycastHit raycastHitInfo, raycastDistance, characterController.layerMask);

                    Debug.DrawLine(raycastOrigin, raycastOrigin + -characterController.localUp * raycastDistance);

                    hangingOnEdge = raycastHitInfo.collider == null || raycastHitInfo.point == Vector3.zero;
                }

                var gravityDirection = GetComputedGravityNormal(controllerIsGrounded, contactNormal, characterController.localUp * -1, hangingOnEdge ? 0.958f : 1f);

                result.gravitationalVelocity += gravityDirection * gravitationalAcceleration * deltaTime;
            }
        }
    }

    private Vector3 GetComputedGravityNormal(bool isGrounded, Vector3 groundingContactNormal, Vector3 baseGravityNormal, float directionInterpolation = 1f)
    {
        var axis = groundingContactNormal.Cross(baseGravityNormal).normalized;
        var angle = Vector3.Angle(-groundingContactNormal, baseGravityNormal);

        return (isGrounded
            ? baseGravityNormal.RotateDirectionAround(axis, angle * directionInterpolation)
            : baseGravityNormal)
            .normalized;
    }
}
